import { switchShown } from "../lib/comments-slider";
import { setCasesCardsListeners } from "../lib/cases-cards";

switchShown();
setCasesCardsListeners();
