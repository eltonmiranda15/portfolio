import {
  getById,
  getByClass,
  getByTag,
  walkerTextNodes
} from "../lib/dom";

export function setCasesCardsListeners() {
  let cards = getByClass('card', getById('cases'));

  for (var i = 0; i < cards.length; i++) {
    cards[i].addEventListener('click', function() {
      this.classList.toggle('is-flipped');
    });
  }
}
