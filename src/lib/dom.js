export function getById(id, parent) {
  try {
    if (parent === undefined) {
      parent = document;
    }
    return parent.getElementById(id);
  } catch (e) {
    console.error('getById Exception\nError: ', e, '\n\nParameters: \n', {
      'id': id,
      'parent': parent
    });
  }
}

export function getByClass(className, parent) {
  try {
    if (parent === undefined) {
      parent = document;
    }
    return parent.getElementsByClassName(className);
  } catch (e) {
    console.error('getByClass Exception\nError: ', e, '\n\nParameters: \n', {
      'className': className,
      'parent': parent
    });
  }
}

export function getByTag(tagName, parent) {
  try {
    if (parent === undefined) {
      parent = document;
    }
    return parent.getElementsByTagName(tagName);
  } catch (e) {
    console.error('getByTag Exception\nError: ', e, '\n\nParameters: \n', {
      'tagName': tagName,
      'parent': parent
    });
  }
}

export function walkerTextNodes(el) {
  var n, a = [],
    walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT, null, false);
  while (n = walk.nextNode()) a.push(n);
  return a;
}
