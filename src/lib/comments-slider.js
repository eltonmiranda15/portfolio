import {
  getById,
  getByClass,
  getByTag,
  walkerTextNodes
} from "./dom";

export function switchShown(interval) {
  let slider, articles, shown, currentShownIdx, nextShownIdx;

  interval = interval || false;

  try {
    slider = getByClass('comments-slider', getById('comments'))[0];

    if (slider === undefined) {
      return;
    }

    articles = getByTag('article', slider);
    shown = getByClass('shown', slider);

    currentShownIdx = Array.from(articles).indexOf(Array.from(shown)[0]);
    nextShownIdx = (currentShownIdx + 1 < articles.length) ? currentShownIdx + 1 : 0;


    articles[currentShownIdx].classList.add('exit-shown');
    setTimeout(() => {
      articles[currentShownIdx].classList.remove('shown');
      articles[currentShownIdx].classList.remove('exit-shown');
      articles[nextShownIdx].classList.add('shown');
    }, 300);

    let howMuchText = walkerTextNodes(articles[nextShownIdx]).map((item) => {
      return item.textContent.trim();
    }).join(' ');

    if(interval === false) {
      interval = howMuchText.length * 25;
    }

    setTimeout(() => {
      switchShown();
    }, interval);
  } catch (e) {
    console.error('switchShown Exception: ', e, '\n\nParameters:\n', {
      'articles': articles,
      'shown': shown,
      'currentShownIdx': currentShownIdx,
      'nextShownIdx': nextShownIdx,
      'articles[currentShownIdx]': articles[currentShownIdx],
      'articles[nextShownIdx]': articles[nextShownIdx]
    });
  }
};
